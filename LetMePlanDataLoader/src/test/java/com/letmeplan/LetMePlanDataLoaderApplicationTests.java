package com.letmeplan;

import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.letmeplan.service.DataLoaderGoogleService;
import com.letmeplan.service.DataLoaderService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LetMePlanDataLoaderApplication.class)
@WebAppConfiguration
public class LetMePlanDataLoaderApplicationTests {

	@Resource
	private DataLoaderService dataLoaderService ; 

	@Resource
	private DataLoaderGoogleService loaderGoogleService; 
	
	private static Logger log = LoggerFactory.getLogger(DataLoaderService.class);
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void loadFactualData() throws Exception{
		String category = "LandMarks";
		String city	= 	"San Francisco";
		String state =	"CA";
		String country = "US";
		dataLoaderService.initDataLoader(category, city, state,country );
	}
	
	@Test
	public void queryPlaceByName() throws Exception{
		String placeName = "Golden Gate";
		Map data = loaderGoogleService.getPlaceByName(placeName);
		log.info(" data " + data);
	}
	
	@Test
	public void loadGooglePlaceId() throws Exception{
		loaderGoogleService.loadData();
	}
	
	@Test
	public void loadGooglePlaceByMeName() throws Exception{
		loaderGoogleService.loadDataForMeNames("Carlsbad");
		loaderGoogleService.loadDataForMeNames("San Diego");
		loaderGoogleService.loadDataForMeNames("Coronado");
		loaderGoogleService.loadDataForMeNames("Del Mar");
		loaderGoogleService.loadDataForMeNames("Escondido");
		loaderGoogleService.loadDataForMeNames("La Jolla");
		loaderGoogleService.loadDataForMeNames("Temecula");
	}
}
