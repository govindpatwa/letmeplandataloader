package com.letmeplan.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="poi_hours")
public class POIHours implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ; 

	@ManyToOne
	@JoinColumn(name="poi_ID")
	private POI poi ;
	
	private Integer day ;
	
	private Double openHour ;
	
	private Double closeHour ;
	
	private Boolean status ;
	
	@Temporal(TemporalType.DATE)
	private Calendar specialDate ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public POI getPoi() {
		return poi;
	}

	public void setPoi(POI poi) {
		this.poi = poi;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Double getOpenHour() {
		return openHour;
	}

	public void setOpenHour(Double openHour) {
		this.openHour = openHour;
	}

	public Double getCloseHour() {
		return closeHour;
	}

	public void setCloseHour(Double closeHour) {
		this.closeHour = closeHour;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Calendar getSpecialDate() {
		return specialDate;
	}

	public void setSpecialDate(Calendar specialDate) {
		this.specialDate = specialDate;
	}

}