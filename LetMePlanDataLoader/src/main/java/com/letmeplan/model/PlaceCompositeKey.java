package com.letmeplan.model;

import java.io.Serializable;

import javax.persistence.Id;

public class PlaceCompositeKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String source; 

	@Id
	private String id ; 

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
