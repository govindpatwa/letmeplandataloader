package com.letmeplan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.letmeplan.model.Category;

@Repository
public interface CategoryRepository  extends GenericDao<Category, String> {

	@Query("select id from Category where active = ?1")
	List<String> findByActive(Boolean active);
	
}