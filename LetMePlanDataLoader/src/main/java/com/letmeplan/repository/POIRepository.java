package com.letmeplan.repository;

import org.springframework.stereotype.Repository;

import com.letmeplan.model.POI;

@Repository
public interface POIRepository  extends GenericDao<POI, Long> {
	
	public POI findFirstByLatitudeAndLongitude(Double latitude,Double longtude); 
	

}