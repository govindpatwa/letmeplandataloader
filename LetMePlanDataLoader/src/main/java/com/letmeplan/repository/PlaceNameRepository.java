package com.letmeplan.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.letmeplan.model.PlaceName;

@Repository
public interface PlaceNameRepository  extends GenericDao<PlaceName, String> {

	Page<PlaceName> findByActiveAndCity(Boolean active,String city,Pageable pageable);
	
	
}