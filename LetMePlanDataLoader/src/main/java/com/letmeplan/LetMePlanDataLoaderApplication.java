package com.letmeplan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LetMePlanDataLoaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(LetMePlanDataLoaderApplication.class, args);
    }
}
