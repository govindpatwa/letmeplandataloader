package com.letmeplan.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.letmeplan.model.POI;
import com.letmeplan.model.POIHours;
import com.letmeplan.model.Place;
import com.letmeplan.model.PlaceCompositeKey;
import com.letmeplan.model.PlaceName;
import com.letmeplan.repository.POIRepository;
import com.letmeplan.repository.PlaceNameRepository;
import com.letmeplan.repository.PlaceRepository;


@Service
public class DataLoaderGoogleService {

	private String API_KEY =  "AIzaSyDyhsX35OkiD4Ez_Fs4pzqthGASf9LdhNU";
	private String placeTextApiUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json";
	
	private String placeDetailApiUrl = "https://maps.googleapis.com/maps/api/place/details/json";
	
	private static Logger log = LoggerFactory.getLogger(DataLoaderGoogleService.class);
	
	@Resource 
	private PlaceRepository placeRepository ;
	
	@Resource
	private POIRepository poiRepository;
	
	@Resource
	private PlaceNameRepository placeNameRepository ;
	
	public void loadData() throws Exception{
		int page = 0 ;
		Pageable pageable = new PageRequest(page , 100);
		
		Page<Place>  places = placeRepository.findByValidateGoogleIsFalseOrValidateGoogleIsNull(pageable );
		while (places.hasNext()) {
			String reason = null ;
			for (Place p : places.getContent()) {
				
				if (p.getValidateGoogle()) {
					log.info(" skipping record " + p);
					continue ;
				}
				reason = null ;
				List<String> types = null ;
				String place_id =  null ;
				String gtypes = null ;
				Boolean validate = true ;
				String formatted_phone_number = null ;
				String address = null ;
				String webSite = null ;
				Double rating = null ;
				Integer noOfReviewer = null ;
				Double gLat = null ;
				Double gLong = null ;
				String gName = null ;
				String error = null ;
				try {
					Map data = getPlaceByName(p.getName());
					JXPathContext context = JXPathContext.newContext(data);
					context.setLenient(true);
					types =  (List<String>) context.getValue(".//types");
					place_id =  (String) context.getValue(".//place_id");
					gtypes = StringUtils.join(types,",");
					rating =  (Double) context.getValue(".//rating",Double.class);
					gLat =  (Double) context.getValue(".//lat",Double.class);
					gLong =  (Double) context.getValue(".//lng",Double.class);
					gName = (String) context.getValue(".//name",String.class);
					
					if (StringUtils.isNoneBlank(place_id)) {
						p.setGooglePlaceId(place_id);
						Map placeDetail = getPlaceById(place_id);
						JXPathContext c = JXPathContext.newContext(placeDetail);
						c.setLenient(true);
						address =  (String) c.getValue(".//formatted_address");
						formatted_phone_number =  (String) c.getValue(".//formatted_phone_number");
						webSite =  (String) c.getValue(".//website");
						noOfReviewer =  (Integer) c.getValue(".//user_ratings_total");
					}
					reason = (String) context.getValue(".//status");
					
				} catch (Exception e) {
					log.error("Error ",e);
					error = e.getMessage();
					p.setGoogleError(error);
					p.setValidateGoogle(true);
					placeRepository.save(p);
				}
				if (StringUtils.equalsIgnoreCase(reason, "OVER_QUERY_LIMIT")) p.setValidateGoogle(false);				
				savePlace(place_id,gtypes,validate,address,webSite,formatted_phone_number,rating,noOfReviewer,gLat,gLong,gName,"FACTUAL",p.getId());
			}
			page++;
			pageable = new PageRequest(page , 100);
			places = placeRepository.findByValidateGoogleIsFalseOrValidateGoogleIsNull(pageable );
		}
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	//@Async
	public void savePlace(String gplcId, String gType, Boolean validateGoogle, String address, String website,String phone, Double rating,Integer noOfReviwer,Double gLat,Double gLong,String gName,String source,String id) {
		placeRepository.updateGoogleData(gplcId, gType, validateGoogle, address, website,phone, rating,noOfReviwer,gLat,gLong,gName,source, id);
	}

	public Map getPlaceById(String placeId) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		String urlStr = placeDetailApiUrl + "?key=" + API_KEY + "&placeid=" +placeId ;
		URI url = new URI(urlStr );
		ResponseEntity<Map> response =  restTemplate.getForEntity(url,Map.class);
		Map data = response.getBody() ;
		log.debug(" Data " + log);
		return data ;
	}
	
	public Map getPlaceByName(String placeName) throws Exception {
		URLCodec codec = new URLCodec();
		RestTemplate restTemplate = new RestTemplate();
		String urlStr = placeTextApiUrl + "?key=" + API_KEY + "&query=" +codec.encode(placeName) ;
		URI url = new URI(urlStr );
		ResponseEntity<Map> response =  restTemplate.getForEntity(url,Map.class);
		Map data = response.getBody() ;
		log.debug(" Data " + log);
		return data ;
	}
	
	
	public void loadDataForMeNames(String city) throws Exception{
		int page = 0 ;
		Pageable pageable = new PageRequest(page , 100);
		
		Page<PlaceName>  places = placeNameRepository.findByActiveAndCity(true,city,pageable);
		while (places.hasContent()) {
			String reason = null ;
			for (PlaceName pName : places.getContent()) {
			try {	
				if (StringUtils.isBlank(pName.getName())) continue ;
				reason = null ;
				List<String> types = null ;
				String place_id =  null ;
				String gtypes = null ;
				Boolean validate = true ;
				String formatted_phone_number = null ;
				String address = null ;
				String webSite = null ;
				Double rating = null ;
				Integer noOfReviewer = null ;
				Double gLat = null ;
				Double gLong = null ;
				String gName = null ;
				String error = null ;
				
				Map data = getPlaceByName(pName.getName() + " " + pName.getCity());
				List result = (List) data.get("results");
				JXPathContext context = JXPathContext.newContext(result);
				context.setLenient(true);
				types =  (List<String>) context.getValue(".//types");
				place_id =  (String) context.getValue(".//place_id");
				gtypes = StringUtils.join(types,",");
				rating =  (Double) context.getValue(".//rating",Double.class);
				gLat =  (Double) context.getValue(".//lat",Double.class);
				gLong =  (Double) context.getValue(".//lng",Double.class);
				gName = (String) context.getValue(".//name",String.class);
				
				List<Place> objs = new ArrayList<Place> () ;
				if (StringUtils.isNotBlank(place_id)) {
					Map placeDetail = getPlaceById(place_id);
					Map detailResult = (Map) placeDetail.get("result");
					JXPathContext c = JXPathContext.newContext(detailResult);
					c.setLenient(true);
					address =  (String) c.getValue(".//formatted_address");
					formatted_phone_number =  (String) c.getValue(".//formatted_phone_number");
					webSite =  (String) c.getValue(".//website");
					noOfReviewer =  (Integer) c.getValue(".//user_ratings_total");
					List<Map>  period =  (List) c.getValue(".//opening_hours/periods");
					reason = (String) context.getValue(".//status");
					
					// save POI 
					POI poi  = poiRepository.findFirstByLatitudeAndLongitude(gLat, gLong); 
					if (poi == null) poi  = new POI(); 
					poi.setName(pName.getName());
					poi.setAddress1(address);
					poi.setCategory(pName.getCategory());
					poi.setCity(city);
					poi.setPhone(formatted_phone_number);					
					poi.setRating(rating);
					poi.setWebsite(webSite);
					poi.setLongitude(gLong);
					poi.setLatitude(gLat);

					
					String[] addCom = address.split(",");
					poi.setState(StringUtils.substringBefore(addCom[addCom.length-2], " "));
					poi.setZipcode(StringUtils.substringAfter(addCom[addCom.length-2], " "));
					poi.setCountry(addCom[addCom.length-1]);
					poi.setCity(city);
					
					// save hours
					List<POIHours> poiHours = poi.getPoiHours(); 
					
					if (poiHours == null) {
						poiHours = new ArrayList<POIHours>();
						if (period != null) {
							for (Map openClose : period) {
								POIHours hours = new POIHours();
								Map open = (Map) openClose.get("open");
								if (open != null) {
									Object  openDay =  open.get("day");
									Object  openTime =  open.get("time");
									hours.setOpenHour(Double.parseDouble(openTime+""));
									hours.setDay((Integer) openDay);
								}
								
								Map close = (Map) openClose.get("close");
								if (close != null) {
									Object  closeTime =  close.get("time");
									hours.setCloseHour(Double.parseDouble(closeTime+""));
								}
								hours.setStatus(true);
								hours.setPoi(poi);
								poiHours.add(hours);
		
							}
						}
					poi.setPoiHours(poiHours);		
					}
					poiRepository.save(poi);
				}
			} catch (Exception ex) {
				log.error(" Error " + pName.getName() + " " + pName.getCity(),ex);
			}
			}
			if (places.hasNext()) places = placeNameRepository.findByActiveAndCity(true,city, places.nextPageable());
			else break ;
		}
	}
	

	public void loadDataForMeNamesOld(String city) throws Exception{
		int page = 0 ;
		Pageable pageable = new PageRequest(page , 100);
		
		Page<PlaceName>  places = placeNameRepository.findByActiveAndCity(true,city,pageable );
		while (places.hasNext()) {
			String reason = null ;
			for (PlaceName pName : places.getContent()) {
				if (StringUtils.isBlank(pName.getName())) continue ;
				
				reason = null ;
				List<String> types = null ;
				String place_id =  null ;
				String gtypes = null ;
				Boolean validate = true ;
				String formatted_phone_number = null ;
				String address = null ;
				String webSite = null ;
				Double rating = null ;
				Integer noOfReviewer = null ;
				Double gLat = null ;
				Double gLong = null ;
				String gName = null ;
				String error = null ;
				String openHours = null;
				
					Map data = getPlaceByName(pName.getName());
					List result = (List) data.get("results");
					JXPathContext context = JXPathContext.newContext(result);
					context.setLenient(true);
					types =  (List<String>) context.getValue(".//types");
					place_id =  (String) context.getValue(".//place_id");
					gtypes = StringUtils.join(types,",");
					rating =  (Double) context.getValue(".//rating",Double.class);
					gLat =  (Double) context.getValue(".//lat",Double.class);
					gLong =  (Double) context.getValue(".//lng",Double.class);
					gName = (String) context.getValue(".//name",String.class);
					List<Place> objs = new ArrayList<Place> () ;
					if (StringUtils.isNoneBlank(place_id)) {					
						objs = placeRepository.findByGooglePlaceId(place_id);
						for (Place p : objs ) {
							p.setMeName(pName.getName());
						 	p.setMeCategory(pName.getCategory());
							placeRepository.save(p);
						}
					}
					
					if (StringUtils.isBlank(place_id)) {
						Place p = new Place();
						p.setId("ME_NAME_" + city);
						p.setSource("ME_NAME_");
						p.setMeName(pName.getName());
						p.setMeCategory(pName.getCategory());
						placeRepository.save(p);
					} else if (objs.isEmpty() && StringUtils.isNotBlank(place_id)) {
						PlaceCompositeKey key = new PlaceCompositeKey();
						key.setSource("GOOGLE");
						key.setId(place_id);
						Place p = placeRepository.findOne(key);
						if (p == null) {
							p = new Place();
							p.setId(place_id);
							p.setSource("GOOGLE");
							
							p.setGooglePlaceId(place_id);
							Map placeDetail = getPlaceById(place_id);
							JXPathContext c = JXPathContext.newContext(placeDetail);
							c.setLenient(true);
							address =  (String) c.getValue(".//formatted_address");
							formatted_phone_number =  (String) c.getValue(".//formatted_phone_number");
							webSite =  (String) c.getValue(".//website");
							noOfReviewer =  (Integer) c.getValue(".//user_ratings_total");
							Object period =  c.getValue(".//opening_hours/periods");
							if (period != null) openHours = period +"";
							
							reason = (String) context.getValue(".//status");
							if (StringUtils.equalsIgnoreCase(reason, "OVER_QUERY_LIMIT")) p.setValidateGoogle(false);				
						}
						p.setMeName(pName.getName());
						p.setMeCategory(pName.getCategory());
						p.setGoogleOpenHours(openHours);
						placeRepository.save(p);
						savePlace(place_id,gtypes,validate,address,webSite,formatted_phone_number,rating,noOfReviewer,gLat,gLong,gName,"GOOGLE",p.getId());
				}
			}
			page = 140;
			pageable = new PageRequest(page , 100); 
			places = placeNameRepository.findByActiveAndCity(true,city, pageable);
		}
	}
}