package com.letmeplan.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


@Service
public class DataLoaderService {

	@Resource
	private DataLoaderFactualService dataLoaderFactualService ;
	
	public Integer initDataLoader(String category,String city,String state,String country) throws Exception {
		return dataLoaderFactualService.loadData(category, city, state, country);
	}

}